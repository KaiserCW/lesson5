<?php

function findStudent($username, $students) {
   if (isset($students[$username])) {
       return $students[$username];
   } else {
       echo 'No such user!';
       return false;
   }
}

function getStudentInfoString($studentInfo) {
    $resultString = "Name : {$studentInfo['name']}\n";
    $resultString .= "Age : {$studentInfo['age']}\n";
    $resultString .= "Gender : {$studentInfo['gender']}\n";
    return $resultString;
}

$students = [
  'peter' => [
      'name' => 'Peter',
      'age' => 25,
      'gender' => 'male'
  ],
  'vasya' => [
      'name' => 'Vasiliy',
      'age' => 28,
      'gender' => 'male'
  ],
  'nina' => [
      'name' => 'Antonina',
      'age' => 21,
      'gender' => 'female'
  ]
];

$username = isset($argv[1]) ? $argv[1] : '';

$studentInfo = findStudent($username, $students);

if ($studentInfo) {
    echo getStudentinfoString($studentInfo);
}