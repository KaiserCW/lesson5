<?php

function calculator($a, $operation, $b) {
// check if user typed valid operation
    if(empty($operation)) {
        echo 'Error: Empty operation!';
        return false;
    }

// check if user typed valid numbers
    if(!is_numeric($a) || !is_numeric($b)) {
        echo 'Error: Bad numbers!';
        return false;
    }

// interpret operation sign from string
    switch ($operation) {
        case '+':
            return $a + $b;
        case '-':
            return $a - $b;
        case '/':
            return $a / $b;
        case '*':
            return $a * $b;
        default:
            echo 'Error: Bad operation!';
            return false;
    }
}


//======================== receive expression from console
$number1 = isset($argv[1]) ? $argv[1] : 0;
$number2 = isset($argv[3]) ? $argv[3] : 0;
$operation = isset($argv[2]) ? $argv[2] : '';

//======================== calling function 'calculator'
$result = calculator($number1, $operation, $number2);

//======================== check if result is valid
if ($result === false) {
   exit;
}
echo $number1 . ' ' . $operation . ' ' . $number2 . ' = ' . $result;
