<?php

$array = range(1, 100);
shuffle($array);

$min = 25;
$max = 32;


/*  array_filter - обходит каждое значение массива array, передавая его в callback-функцию.
 *  Если callback-функция возвращает true, данное значение из array возвращается в результирующий массив.
 *  Ключи массива сохраняются. */

$filteredArray = array_filter($array, function($value) use ($min, $max) {
    if ($value > $min && $value < $max) {
        return true;
    }
});

print_r($filteredArray);

/*  array_map — Применяет callback-функцию ко всем элементам указанных массивов */

$doubledArray = array_map(function($value) {
    return $value ** 2;
  },
  $filteredArray
);

print_r($doubledArray);
