<?php

/*
//============================ creating array with numbers sequence and shuffle it

$array = range(1, 50);
shuffle($array);

//============================ make some print manipulations with array:

    //---------- print shuffled array
var_export($array) . PHP_EOL;

    //---------- print first (func current()) and last (func end()) elements of array
echo 'First element in array: ' . current($array) . PHP_EOL, 'Last element in array: ' . end($array) . PHP_EOL;

    //---------- count and print number of elements in array with func count()
echo 'Number of elements in array: ' . count($array) . PHP_EOL;

    //---------- convert all array's elements in string (implode()) and print them
echo 'All array\'s elements: ' . implode(', ', $array);



//============================ variable with some string (Lorem ipsum, for example)

$lorem = 'Lorem ipsum dolor sit amet.';

    //---------- break string by spaces (create array of worlds) with func explode() and print resulting array
$loremArr = explode(' ', $lorem);
var_export($loremArr);

*/


//============================ create two arrays in different ways:

$arrByPushed = [];
$arrByClassic = [];

    //---------- using func array_push
array_push($arrByPushed, 'Brad', 'Pitt', 53);

    //---------- created in classic way
$arrByClassic = [
  'name' => 'Angelina',
  'surname' => 'Jolie',
  'current age' => '41'
];

echo 'Array created by using array_push function:' . PHP_EOL;
print_r($arrByPushed);

echo 'Array created in classic way:' . PHP_EOL;
print_r($arrByClassic);

    //---------- merge two created arrays and print resulting array
$mergedArr = array_merge($arrByPushed, $arrByClassic);

echo 'Print resulting(merged) array:' . PHP_EOL;
print_r($mergedArr);
