<?php

function findStudent($username, $students) {
    if (isset($students[$username])) {
        return $students[$username];
    } else {
        echo 'No such user!';
        return false;
    }
}

function getStudentInfoString($studentInfo) {
    $resultString = "Name : {$studentInfo['name']}\n";
    $resultString .= "Age : {$studentInfo['age']}\n";
    $resultString .= "Gender : {$studentInfo['gender']}\n";
    $resultString .= "Lessons : " . implode(' | ', $studentInfo['lessons']);
    return $resultString;
}

//===============================
$students = [
  'peter' => [
      'name' => 'Peter',
      'age' => 25,
      'gender' => 'male',
      'lessons' => []
    ],
  'vasya' => [
      'name' => 'Vasiliy',
      'age' => 28,
      'gender' => 'male',
      'lessons' => []
    ],
  'nina' => [
      'name' => 'Antonina',
      'age' => 21,
      'gender' => 'female',
      'lessons' => []
    ]
];

$progLangs = [
    "PHP",
    "JavaScript",
    "Golang",
    "Ruby",
    "Perl",
    "Python",
    "Java",
    "C#",
    "C++"
];

//===============================
$students['peter']['lessons'] = [$progLangs[2], $progLangs[6]];
$students['vasya']['lessons'] = [$progLangs[0], $progLangs[1], $progLangs[4], $progLangs[6], $progLangs[8]];
$students['marge']['lessons'] = [$progLangs[1], $progLangs[3], $progLangs[7]];

$username = strtolower(isset($argv[1]) ? $argv[1] : '');

$studentInfo = findStudent($username, $students);

//print_r($studentInfo);

if ($studentInfo) {
    echo getStudentinfoString($studentInfo);
}
